﻿using dotNet_Core_Groepsopdracht.Areas.Identity.Data;
using dotNet_Core_Groepsopdracht.Attributes;
using dotNet_Core_Groepsopdracht.Data.Database;
using dotNet_Core_Groepsopdracht.Entities;
using dotNet_Core_Groepsopdracht.Models;
using dotNet_Core_Groepsopdracht.Services.Design;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace dotNet_Core_Groepsopdracht.Controllers {
	public class NewsController : Controller {
		private readonly IDatabase _database;
		private readonly UserManager<User> _userManager;
		private readonly IPhotoService _photoService;

		public NewsController(IDatabase database, UserManager<User> userManager, IPhotoService photoService) {
			_database = database;
			_userManager = userManager;
			_photoService = photoService;
		}

		public IActionResult Index( ) {
			List<NewsIndexViewModel> viewModelList = new List<NewsIndexViewModel>();

			foreach (NewsItem x in _database.GetNewsItems( )) {
				NewsIndexViewModel newModel = new NewsIndexViewModel {
					Title = x.Title,
					Content = x.Content,
					Id = x.Id,
					ImageUrl = x.ImageUrl
				};
				viewModelList.Add(newModel);
			}

			return View(viewModelList);
		}

		[HttpGet]
		[PermissionAuthorize(Permissions.CreateNewsItem, PermissionAuthorizeType.HasAny)]
		public IActionResult Create( ) {
			NewsCreateViewModel newsitem = new NewsCreateViewModel();
			return View(newsitem);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		[PermissionAuthorize(Permissions.CreateNewsItem, PermissionAuthorizeType.HasAny)]
		public IActionResult Create([FromForm] NewsCreateViewModel viewModel) {
			if (TryValidateModel(viewModel)) {
				string imageUrl = null;
				if (viewModel.Image != null) imageUrl = _photoService.UploadedFile(viewModel.Image);
				NewsItem newsItem = new NewsItem()
				{
					Id = Guid.NewGuid(),
					Content = viewModel.Content,
					Title = viewModel.Title,
					Author = _userManager.GetUserAsync(User).Result,
					DateCreated = DateTime.Now,
					Reactions = new List<Reaction>(),
					ImageUrl = imageUrl
				};

				_database.AddNewsItem(newsItem);
				return RedirectToAction(nameof(Index));
			} else {
				return View(viewModel);
			}
		}



		[HttpGet]
		public IActionResult Edit(Guid id) {
			NewsItem newsitemToEdit = _database.FindNewsItemById(id);
			if (newsitemToEdit == null) return new NotFoundResult( );

			User user = _userManager.GetUserAsync(User).Result;

			if (user is null) {

				return RedirectToAction(nameof(Detail), new { id });
			}
			user = _database.FindUserById(user.Id);
			//check if news is yours or not if it aint do you have permission to edit others newsarticle?
			if (newsitemToEdit.AuthorId != user.Id || user.HasPerm(Permissions.EditNewsItem)) //if NOT your NewsArticle
			{

				NewsEditViewModel newsEditViewModel = new NewsEditViewModel()
					{
					Id = newsitemToEdit.Id,
					Title = newsitemToEdit.Title,
					Content = newsitemToEdit.Content,
					ImageUrl = newsitemToEdit.ImageUrl
				};
				return View(newsEditViewModel);
			} else {
				return RedirectToAction(nameof(Detail), new { id });
			}
		}

		[HttpPost]
		public IActionResult Edit(Guid id, [FromForm] NewsEditViewModel newsEditViewModel) {
			if (!TryValidateModel(newsEditViewModel)) return View(newsEditViewModel);
			NewsItem newsitemToEdit = _database.FindNewsItemById(id);
			newsitemToEdit.Title = newsEditViewModel.Title;
			newsitemToEdit.DateAdjusted = DateTime.Now;
			newsitemToEdit.Content = newsEditViewModel.Content;
			if (newsEditViewModel.Image != null) newsitemToEdit.ImageUrl = _photoService.UploadedFile(newsEditViewModel.Image);
			// Id, DateCreated, Author (User) and Reactions cannot be changed
			_database.UpdateNewsItem(newsitemToEdit, id);
			return RedirectToAction(nameof(Detail), new { id });
		}

		[HttpGet]
		public IActionResult Detail(Guid id) {
			NewsItem newsItem = _database.FindNewsItemById(id);
			List<NewsDetailReactionViewModel> reactions = newsItem.Reactions
				.OrderByDescending(x => x.DateMade)
				.Select(x => new NewsDetailReactionViewModel {
					Id = x.Id,
					Title = x.Title,
					DateMade = x.DateMade,
					AuthorId = x.Author.Id,
					AuthorUserName = x.Author.UserName,
					Content = x.Content
				}).ToList( );

			return View(new NewsDetailViewModel {
				Id = id,
				Title = newsItem.Title,
				AuthorId = newsItem.Author.Id,
				DateCreated = newsItem.DateCreated,
				DateAdjusted = newsItem.DateAdjusted,
				Content = newsItem.Content,
				ImageUrl = newsItem.ImageUrl,
				AuthorUserName = newsItem.Author.UserName,
				Reactions = reactions
			});
		}

		[HttpPost]
		public IActionResult Detail(Guid id, NewsDetailViewModel viewModel) {
			NewsItem newsItem = _database.FindNewsItemById(id);
			if (!TryValidateModel(viewModel)) {
				List<NewsDetailReactionViewModel> reactions = newsItem.Reactions.Select(x => new NewsDetailReactionViewModel {
					Title = x.Title,
					DateMade = x.DateMade,
					AuthorId = x.Author.Id,
					AuthorUserName = x.Author.UserName,
					Content = x.Content
				}).ToList( );
				return View(new NewsDetailViewModel {
					Title = newsItem.Title,
					AuthorId = newsItem.Author.Id,
					DateCreated = newsItem.DateCreated,
					DateAdjusted = newsItem.DateAdjusted,
					Content = newsItem.Content,
					ImageUrl = newsItem.ImageUrl,
					AuthorUserName = newsItem.Author.UserName,
					Reactions = reactions,
					ReactionTitle = viewModel.ReactionTitle,
					Reaction = viewModel.Reaction
				});
			}
			Reaction reaction = new Reaction(){
				Id = Guid.NewGuid(),
				Title = viewModel.ReactionTitle,
				DateMade = DateTime.Now,
				Author = _userManager.GetUserAsync(User).Result,
				Content = viewModel.Reaction,
				NewsItem = _database.FindNewsItemById(id)
			};
			_database.AddReaction(reaction);
			//newsItem.Reactions.Add(reaction);
			//_database.UpdateNewsItem(newsItem, id);
			return RedirectToAction("Detail", new { id = id });

		}


		[HttpGet]
		public IActionResult Delete(Guid id) {
			NewsItem newsToDelete = _database.FindNewsItemById(id);
			if (newsToDelete == null) return new NotFoundResult( );

			NewsDeleteViewModel newsDeleteViewModel = new NewsDeleteViewModel(){
				Id = newsToDelete.Id,
				Title = newsToDelete.Title,
				DateCreated = newsToDelete.DateCreated,
				Content = newsToDelete.Content,
				AuthorUserName = newsToDelete.Author.UserName,
			};
			return View(newsDeleteViewModel);
		}

		[HttpPost]
		public IActionResult ConfirmDelete(Guid id) {
			NewsItem newsToDelete = _database.FindNewsItemById(id);
			if (newsToDelete == null) return new NotFoundResult( );

			_database.RemoveNewsItem(newsToDelete);


			return RedirectToAction("Index");
		}

		[HttpGet]
		public IActionResult DeleteReaction(Guid id) {
			Reaction reactionToDelete = _database.FindReactionById(id);
			if (reactionToDelete == null) return new NotFoundResult( );
			NewsItem sourceNewsItem = _database.FindNewsItemById(reactionToDelete.NewsItemId);
			_database.RemoveReaction(reactionToDelete);
			return RedirectToAction("Detail", new { id = sourceNewsItem.Id });
		}
	}
}