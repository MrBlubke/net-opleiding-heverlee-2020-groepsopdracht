using dotNet_Core_Groepsopdracht.Attributes;
using dotNet_Core_Groepsopdracht.Data.Database;
using dotNet_Core_Groepsopdracht.Entities;
using dotNet_Core_Groepsopdracht.Models;
using dotNet_Core_Groepsopdracht.Services.Design;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Security.Permissions;


namespace dotNet_Core_Groepsopdracht.Controllers {

	public class EventController : Controller {
		private readonly IWebHostEnvironment _webHostEnvironment;
		private readonly IDatabase _database;
		private readonly IPhotoService _photoService;
		//private static readonly List<PeriodViewModel> nieuweLijst = new List<PeriodViewModel>();

		public EventController(IWebHostEnvironment webhostEnvironment, IDatabase database, IPhotoService photoService) {
			_webHostEnvironment = webhostEnvironment;
			_database = database;
			_photoService = photoService;
		}

		//[HttpGet]
		public IActionResult Index( ) {
			List<OverviewEventViewModel> lijst = new List<OverviewEventViewModel>();

			foreach (Event eventEntity in _database.GetEvents( )
												   .GroupBy(e => e.Periods[^1].End < DateTime.Now)
												   .OrderBy(x => x.Key)
												   .SelectMany(x => x.Key ?
														x.OrderByDescending(e => e.Periods[^1].End) :
														x.OrderBy(e => e.Periods[0].Start))) {
				lijst.Add(new OverviewEventViewModel {
					Id = eventEntity.Id,
					Title = eventEntity.Title,
					Address = eventEntity.Address is null ? new AddressViewModel( ) :
						new AddressViewModel {
							Id = eventEntity.Address.Id,
							Street = eventEntity.Address.Street,
							Number = eventEntity.Address.Number,
							Bus = eventEntity.Address.Bus,
							ZipCode = eventEntity.Address.ZipCode,
							City = eventEntity.Address.City
						},
					PeriodeEvenement = new DateTime[0], // TODO: add this
					Description = eventEntity.Description,
					Image = eventEntity.Image,
					Price = eventEntity.Price,
					Sponsors = new List<SponsorViewModel>( ), // TODO: add sponsors
					MaxPlacesLeft = eventEntity.MaxPlacesLeft( )
				});
			}

			return View(lijst);
		}

		//[HttpGet]
		//public IActionResult Detail( ) {
		//	return View( );
		//}

		[HttpGet]
		[PermissionAuthorize(Permissions.EventManagement)]
		public IActionResult Create( ) {
			EventCreateViewModel ecvm = new EventCreateViewModel();
			ecvm.Number = 1;//zet de input al op min 1 want de value in input (Create.cshtml) werkt natuurlijk niet
			ecvm.PeriodLijst.Add(new PeriodViewModel( ) { Id = Guid.NewGuid( ), RemoveRemoveButton = true });
			return View(ecvm);
		}

		[HttpPost]
		[PermissionAuthorize(Permissions.EventManagement)]
		public IActionResult Create([FromForm] EventCreateViewModel ecvm) {
			if (TryValidateModel(ecvm)) {
				List<Period> periods = new List<Period>();

				Address newAdress = new Address{
					Street = ecvm.Street,
					Number = ecvm.Number,
					Bus = ecvm.Bus,
					City = ecvm.City,
					ZipCode = ecvm.ZipCode,
					Id = Guid.NewGuid()
				};
				Event newEvent = new Event{
					Id = Guid.NewGuid(),
					Title = ecvm.Title,
					MaxCap = ecvm.MaxCap,
					MinCap = ecvm.MinCap,
					Description = ecvm.Description,
					Address = newAdress,
					Image = _photoService.UploadedFile(ecvm.Image),
					Price = ecvm.Price
				};


				foreach (PeriodViewModel pvm in ecvm.PeriodLijst) {
					if (pvm == null) {
						ModelState.AddModelError("Periode", "Je moet de periode invullen");
						return View(ecvm);
					}
					if (!ecvm.PeriodIsCorrect(pvm)) {
						ModelState.AddModelError("Periode", "Startdatum periode kan niet later zijn dan de einddatum van dezelfde periode");
						return View(ecvm);
					}

					//checkt period for correct start en end
					Period newPeriod = new Period
				{
						Start = pvm.Start,
						End = pvm.End,
						//Id = Guid.NewGuid(), // TO CHECK: All PeriodID's in the DB are the same for now!! Is that the intention?
						Event = newEvent
					};
					periods.Add(newPeriod);
				}
				newEvent.Periods = periods;
				//add event to db:
				_database.AddEvent(newEvent);
				return RedirectToAction("Index");
			} else {
				return View(ecvm);
			}
		}

		[HttpGet]
		[PermissionAuthorize(Permissions.EventManagement)]
		public IActionResult Edit(Guid id) {
			Event eventToEdit = _database.FindEventById(id);
			if (eventToEdit == null) return new NotFoundResult( );

			List<SelectListItem> sponsortags = new List<SelectListItem>();
			foreach (Sponsor sponsor in _database.GetSponsors( )) {
				sponsortags.Add(new SelectListItem { Value = sponsor.Naam, Text = sponsor.Naam });
			}

			EventEditViewModel eventEditViewModel = new EventEditViewModel()
			{
				Id = eventToEdit.Id,
				Title = eventToEdit.Title,
				MaxCap = eventToEdit.MaxCap,
				MinCap = eventToEdit.MinCap,
				Street = eventToEdit.Address is null ? null : eventToEdit.Address.Street,
				Number = eventToEdit.Address is null ? 0 : eventToEdit.Address.Number,
				Bus = eventToEdit.Address is null ? null : eventToEdit.Address.Bus,
				ZipCode = eventToEdit.Address is null ? null : eventToEdit.Address.ZipCode,
				City = eventToEdit.Address is null ? null : eventToEdit.Address.City,
				CurrentImgPath=eventToEdit.Image,
				Description = eventToEdit.Description,
				PeriodLijst=new List<PeriodViewModel>(),
				Price = eventToEdit.Price,
				SelectedSponsors= eventToEdit.SponsorEvents is null ? null : eventToEdit.SponsorEvents.Select(x=> x.Sponsor.Naam).ToArray()
			};
			foreach (Period period in eventToEdit.Periods) {
				PeriodViewModel pvm = new PeriodViewModel{
					Id = period.Id,
					Start = period.Start,
					End = period.End
				};
				eventEditViewModel.PeriodLijst.Add(pvm);
			}
			eventEditViewModel.Sponsortags = sponsortags;
			return View(eventEditViewModel);
		}
		[HttpPost]
		[PermissionAuthorize(Permissions.EventManagement)]
		public IActionResult Edit(Guid id, [FromForm] EventEditViewModel eevm) {

			if (TryValidateModel(eevm)) {
				List<SelectListItem> sponsortags = new List<SelectListItem>();

				foreach (Sponsor sponsor in _database.GetSponsors( )) {
					sponsortags.Add(new SelectListItem { Value = sponsor.Naam, Text = sponsor.Naam });
				}

				//convert viewmodel to event entity:
				Address newAdress = new Address{
					Street = eevm.Street,
					Number = eevm.Number,
					Bus = eevm.Bus,
					City = eevm.City,
					ZipCode = eevm.ZipCode
				};
				Event newEvent = _database.FindEventById(id);
				//newEvent.Id = eevm.Id;
				newEvent.Title = eevm.Title;
				newEvent.MaxCap = eevm.MaxCap;
				newEvent.MinCap = eevm.MinCap;
				newEvent.Description = eevm.Description;
				newEvent.Address = newAdress;
				newEvent.Price = eevm.Price;
				newEvent.Periods = new List<Period>( );
				newEvent.SponsorEvents = new List<SponsorEvent>( );

				if (eevm.Image != null) {
					string uniquefileName = _photoService.UploadedFile(eevm.Image);
					newEvent.Image = uniquefileName;
				}
				if (eevm.PeriodLijst == null) {
					eevm.PeriodLijst = new List<PeriodViewModel>( );
				}
				foreach (PeriodViewModel pvm in eevm.PeriodLijst) {
					if (pvm != null) {
						if (eevm.PeriodIsCorrect(pvm)) {//checkt period for correct start en end
							Period newPeriod = new Period{
								Start = pvm.Start,
								End = pvm.End,
								Event = newEvent
							};
							newEvent.Periods.Add(newPeriod);
						} else {
							ModelState.AddModelError("Periode", "Startdatum periode kan niet later zijn dan de einddatum van dezelfde periode.");
							return View(eevm);
						}
					} else {
						ModelState.AddModelError("Periode", "Je moet de periode invullen.");
						return View(eevm);
					}
				}
				IEnumerable<Sponsor> sponsorList= _database.GetSponsors();

				IEnumerable<Sponsor> selectedSponsors = eevm.SelectedSponsors is null? new List<Sponsor>() : sponsorList.Where(s => eevm.SelectedSponsors.Contains(s.Naam));
				foreach (Sponsor sponsor in selectedSponsors) {
					newEvent.SponsorEvents.Add(new SponsorEvent {
						Event = newEvent,
						EventId = newEvent.Id,
						Sponsor = sponsor,
						SponsorId = sponsor.Id,
					});
				}
				eevm.Sponsortags = sponsortags;
				//update database:
				_database.UpdateEvent(newEvent, newEvent.Id);

				return RedirectToAction("Index");
			} else { return View(eevm); }
		}
		[HttpGet]
		[PermissionAuthorize(Permissions.EventManagement)]
		public IActionResult Delete(Guid id) {
			Event eventToDelete = _database.FindEventById(id);
			if (eventToDelete == null) return new NotFoundResult( );

			EventDeleteViewModel eventDeleteViewModel = new EventDeleteViewModel()
				{
				Title = eventToDelete.Title,
				Id=eventToDelete.Id

			};
			return View(eventDeleteViewModel);
		}

		[HttpPost]
		[PermissionAuthorize(Permissions.EventManagement)]
		public IActionResult ConfirmDelete(Guid id) {
			Event eventToDelete = _database.FindEventById(id);
			if (eventToDelete == null) return new NotFoundResult( );

			_database.RemoveEvent(eventToDelete);

			//if (!_database.FindEventById(id)) return new NotFoundResult( );//dit stuk snap ik niet

			return RedirectToAction("Index");
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult AddPeriodViewModelToEvent([Bind("PeriodLijst")] EventCreateViewModel ecvm) {
			ecvm.PeriodLijst.Add(new PeriodViewModel { Id = Guid.NewGuid( ) });
			return PartialView("PeriodeViewModel", ecvm);
		}


		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult AddPeriodViewModelToEditEvent([Bind("PeriodLijst")] EventEditViewModel eevm) {
			if (eevm.PeriodLijst == null) { eevm.PeriodLijst = new List<PeriodViewModel>( ); }
			eevm.PeriodLijst.Add(new PeriodViewModel { Id = Guid.NewGuid( ) });
			return PartialView("PeriodEditViewModel", eevm);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult RemovePeriodViewModelFromEvent(Guid id, [FromForm] EventCreateViewModel ecvm) {

			PeriodViewModel pvm = ecvm.PeriodLijst.FirstOrDefault(x => x.Id == id);
			if (ecvm.PeriodLijst.Remove(pvm)) return Json(new { succes = true, responseText = "" });
			else return Json(new { succes = false, responseText = "Periode is niet gevonden." });
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		//event period remove
		public IActionResult RemovePeriodViewModelFromEditEvent(Guid id, [FromForm] EventEditViewModel eevm) {

			PeriodViewModel pvm = eevm.PeriodLijst.FirstOrDefault(x => x.Id == id);

			if (eevm.PeriodLijst.Remove(pvm)) return Json(new { succes = true, responseText = "" });
			else return Json(new { succes = false, responseText = "Periode is niet gevonden." });

		}

		public IActionResult Detail(Guid id) {



			Event selectedEvent = _database.FindEventById(id);

			Address address = selectedEvent.Address;
			List<Period> period = selectedEvent.Periods;
			IEnumerable<Sponsor>sponsors = selectedEvent.SponsorEvents.Select(x=>x.Sponsor);

			EventDetailViewModel eventViewModel = new EventDetailViewModel
				{
				Id = selectedEvent.Id,
				Title = selectedEvent.Title,
				MaxCap = selectedEvent.MaxCap,
				MinCap = selectedEvent.MinCap,
				Description = selectedEvent.Description,
				Image = selectedEvent.Image,
				Price = selectedEvent.Price,
				Street = address.Street,
				Number = address.Number,
				Bus = address.Bus,
				ZipCode = address.ZipCode,
				City = address.City
			};


			foreach (Period item in period) {

				EventPeriodViewModel e = new EventPeriodViewModel();
				e.Start = item.Start;
				e.End = item.End;

				eventViewModel.Periods.Add(e);
			}

			foreach (Sponsor sponsor in sponsors) {

				SponsorViewModel e = new SponsorViewModel();
				e.Naam = sponsor.Naam;
				e.Photo = sponsor.Photo;

				eventViewModel.Sponsors.Add(e);
			}

			return View(eventViewModel);
		}
	}
}