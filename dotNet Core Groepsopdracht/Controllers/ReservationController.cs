﻿using dotNet_Core_Groepsopdracht.Areas.Identity.Data;
using dotNet_Core_Groepsopdracht.Data.Database;
using dotNet_Core_Groepsopdracht.Database;
using dotNet_Core_Groepsopdracht.Entities;
using dotNet_Core_Groepsopdracht.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace dotNet_Core_Groepsopdracht.Controllers {

	public class ReservationController : Controller {
		private readonly IDatabase _database;
		private readonly UserManager<User> _userManager;

		public ReservationController(IDatabase database, UserManager<User> userManager) {
			_database = database;
			_userManager = userManager;
		}

		/// <summary>
		/// Displays the reservations of a logged in user
		/// </summary>
		/// <returns></returns>
		[Authorize]
		public IActionResult Index( ) {
			var loggedInReservationsList = new List<ReservationIndexViewModel>();
			var userId = _userManager.GetUserAsync(User).Result.Id;
			var reservationsOfUser = _database.FindReservationsByUserId(userId).OrderBy(r => r.ReservationDateTime);
			foreach (var reservation in reservationsOfUser) {
				loggedInReservationsList.Add(ToListViewModel(reservation));
			}
			return View(loggedInReservationsList);
		}

		public ReservationIndexViewModel ToListViewModel(ReservationLoggedIn reservationLoggedIn) {
			return new ReservationIndexViewModel {
				Id = reservationLoggedIn.Id,
				EventId = reservationLoggedIn.EventId,
				EventTitle = _database.FindEventById(reservationLoggedIn.EventId).Title,
				ReservationDateTime = reservationLoggedIn.ReservationDateTime,
				CreatedDateTime = reservationLoggedIn.CreatedDateTime,
				//UserName = reservationLoggedIn.User.UserName,
				NumberOfPeople = reservationLoggedIn.NumberOfPeople,
				//ReservationDate = reservationLoggedIn.ReservationDateTime.Date.ToShortDateString( ),
				//ReservationTime = reservationLoggedIn.ReservationDateTime.TimeOfDay.ToString( ),
			};
		}

		[HttpGet]
		public IActionResult Create(Guid id) {
			var _event = _database.FindEventById(id);
			if (_event == null) return new NotFoundResult( );

			// Dropdown list for possible dates
			//List<string[]> periods = new List<string[]>();
			List<string> periods = new List<string>();
			List<SelectListItem> possibleDates = new List<SelectListItem>();
			// TODO: Dropdown list is now displaying all dates, but not the hours yet!
			foreach (var period in _database.FindPeriodsByEventId(id)) {
				//for (var date = period.Start.Date; date <= period.End.Date; date = date.AddDays(1.0)) {
				string txt = period.Start.ToShortDateString() + " " + period.Start.ToString("HH:mm");
				if (period.Start != period.End) txt = txt + "-" + period.End.ToString("HH:mm");
				var newItem = new SelectListItem {Value = period.Start.Date.ToString(), Text = txt};
				possibleDates.Add(newItem);
				//}
				string periodDay = period.Start.Date.ToString();
				string periodStartH = period.Start.ToString("HH");
				//string periodStartH2 = period.Start.Hour.ToString();
				string periodStartM = period.Start.ToString("mm");
				string periodEndH = period.End.ToString("HH");
				string periodEndM = period.End.ToString("mm");
				periods.Add($"[ '{periodDay}', {periodStartH}, {periodStartM}, {periodEndH}, {periodEndM}, {period.Start.Year}, {period.Start.Month}, {period.Start.Day}]");

			}

			var createLoggedInReservationViewModel = new ReservationCreateLoggedInViewModel()
				{
				EventId = _event.Id,
				EventTitle = _event.Title,
				PossibleDates = possibleDates,
				Periods = periods
			};

			var createNotLoggedInReservationViewModel = new ReservationCreateNotLoggedInViewModel()
				{
				EventId = _event.Id,
				EventTitle = _event.Title,
				PossibleDates = possibleDates,
				Periods = periods
			};

			if (!User.Identity.IsAuthenticated) return View("CreateNotLoggedInReservation", createNotLoggedInReservationViewModel);

			var loggedInUser = _userManager.GetUserAsync(User).Result;
			var user = $"{loggedInUser.FirstName} {loggedInUser.LastName}"; // TODO: For now, only shows part of the emailadres
			createLoggedInReservationViewModel.UserName = user;
			return View("CreateLoggedInReservation", createLoggedInReservationViewModel);
		}

		[HttpPost]
		public IActionResult CreateLoggedIn([FromForm] ReservationCreateLoggedInViewModel reservationViewModel) {
			if (!TryValidateModel(reservationViewModel)) {
				List<string> periods = new List<string>();
				List<SelectListItem> possibleDates = new List<SelectListItem>();
				foreach (var period in _database.FindPeriodsByEventId(reservationViewModel.EventId)) {
					string txt = period.Start.ToShortDateString() + " " + period.Start.ToString("HH:mm");
					if (period.Start != period.End) txt = txt + "-" + period.End.ToString("HH:mm");
					var newItem = new SelectListItem {Value = period.Start.Date.ToString(), Text = txt};
					possibleDates.Add(newItem);
					string periodDay = period.Start.Date.ToString();
					string periodStartH = period.Start.ToString("HH");
					string periodStartM = period.Start.ToString("mm");
					string periodEndH = period.End.ToString("HH");
					string periodEndM = period.End.ToString("mm");
					periods.Add("[ '" + periodDay + "', " + periodStartH + ", " + periodStartM + ", " + periodEndH + ", " + periodEndM + ", " +
						period.Start.ToString("yyyy") + ", " + period.Start.ToString("MM") + ", " + period.Start.ToString("dd") + " ]");
				}
				reservationViewModel.Periods = periods;
				reservationViewModel.PossibleDates = possibleDates;
				return View("CreateLoggedInReservation", reservationViewModel);
			}
			DateTime reservationDateTime = DateTime.Parse(reservationViewModel.ReservationTime, null, System.Globalization.DateTimeStyles.RoundtripKind);
			reservationDateTime = reservationDateTime.AddHours(1); // Add hour becouse time is passed in UTC and we are +1
			var loggedInReservation = new ReservationLoggedIn() {
				Id = Guid.NewGuid(),
				User = _userManager.GetUserAsync(User).Result,
				ReservationDateTime = reservationDateTime,
				CreatedDateTime = DateTime.Now,
				Event = _database.FindEventById(reservationViewModel.EventId),
				NumberOfPeople = reservationViewModel.NumberOfPeople,
			};

			_database.AddReservationLoggedIn(loggedInReservation);

			return RedirectToAction("Index");
		}

		[HttpPost]
		public IActionResult CreateNotLoggedIn([FromForm] ReservationCreateNotLoggedInViewModel reservationViewModel) {
			if (!TryValidateModel(reservationViewModel)) {
				List<string> periods = new List<string>();
				List<SelectListItem> possibleDates = new List<SelectListItem>();
				foreach (var period in _database.FindPeriodsByEventId(reservationViewModel.EventId)) {
					string txt = period.Start.ToShortDateString() + " " + period.Start.ToString("HH:mm");
					if (period.Start != period.End) txt = txt + "-" + period.End.ToString("HH:mm");
					var newItem = new SelectListItem {Value = period.Start.Date.ToString(), Text = txt};
					possibleDates.Add(newItem);
					string periodDay = period.Start.Date.ToString();
					string periodStartH = period.Start.ToString("HH");
					string periodStartM = period.Start.ToString("mm");
					string periodEndH = period.End.ToString("HH");
					string periodEndM = period.End.ToString("mm");
					periods.Add("[ '" + periodDay + "', " + periodStartH + ", " + periodStartM + ", " + periodEndH + ", " + periodEndM + ", " +
						period.Start.ToString("yyyy") + ", " + period.Start.ToString("MM") + ", " + period.Start.ToString("dd") + " ]");
				}
				reservationViewModel.Periods = periods;
				reservationViewModel.PossibleDates = possibleDates;
				return View("CreateNotLoggedInReservation", reservationViewModel);
			}
			DateTime reservationDateTime = DateTime.Parse(reservationViewModel.ReservationTime, null, System.Globalization.DateTimeStyles.RoundtripKind);
			reservationDateTime = reservationDateTime.AddHours(1); // Add hour becouse time is passed in UTC and we are +1
			var notLoggedInReservation = new ReservationNotLoggedIn() {
				Id = Guid.NewGuid(),
				ReservationDateTime = reservationDateTime,
				CreatedDateTime = DateTime.Now,
				Event = _database.FindEventById(reservationViewModel.EventId),
				NumberOfPeople = reservationViewModel.NumberOfPeople,
				FirstName = reservationViewModel.FirstName,
				LastName = reservationViewModel.LastName,
				Email = reservationViewModel.Email,
				PhoneNumber = reservationViewModel.PhoneNumber,
				Zipcode = reservationViewModel.Zipcode,
			};

			// TODO: Fix DB: It gets stuck on SaveChanges() => Duplicate entry exception for primary key
			// Eindigt op login pagina ... ??
			// User is missing in DB!!
			_database.AddReservationNotLoggedIn(notLoggedInReservation);

			return RedirectToAction("ConfirmNotLoggedIn", new { id = notLoggedInReservation.Id });
		}

		/// <summary>
		/// Displays the reservation that a not-logged-in user just made
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		[HttpGet]
		public IActionResult ConfirmNotLoggedIn(Guid id) {
			var notLoggedInReservation = _database.FindReservationNotLoggedInById(id);
			if (notLoggedInReservation == null) return new NotFoundResult( );
			string reservationDate = notLoggedInReservation.ReservationDateTime.ToString("f", CultureInfo.CreateSpecificCulture("nl-BE"));
			var reservationViewModel = new ReservationCreateNotLoggedInViewModel {
				//TODO: Add ReservationDateTime property
				//ReservationDate = notLoggedInReservation.ReservationDateTime.ToString(),

				FirstName = notLoggedInReservation.FirstName,
				LastName = notLoggedInReservation.LastName,
				Email = notLoggedInReservation.Email,
				ReservationDate = reservationDate,
				NumberOfPeople = notLoggedInReservation.NumberOfPeople,
				EventId = notLoggedInReservation.EventId,
				//Event = _database.FindEventById(EventId),
				EventTitle = _database.FindEventById(notLoggedInReservation.EventId).Title,
			};
			return View(reservationViewModel);
		}

		[HttpGet]
		public IActionResult List(Guid Id) {
			var user = _database.FindUserById(Id);
			if (user == null) {
				return new NotFoundResult( );
			}

			var userViewModel = new UserReservationListViewModel
			{
				UserId = user.UserId,
				FirstName=user.FirstName,
				LastName=user.LastName,
				Reservations=user.Reservations
			};

			return View(userViewModel);
		}

		[HttpGet]
		public IActionResult EventList(Guid Id) {
			var selectedEvent = _database.FindEventById(Id);
			if (selectedEvent == null) {
				return new NotFoundResult( );
			}

			var viewModel = new ReservationEventListViewModel {
				EventId = selectedEvent.Id,
				EventTitle = selectedEvent.Title,
				Address = $"{selectedEvent.Address.Street} {selectedEvent.Address.Number} {selectedEvent.Address.Bus ?? ""} \n" +
						  $"{selectedEvent.Address.ZipCode} {selectedEvent.Address.City}",
				Image = selectedEvent.Image
			};

			viewModel.Reservations = selectedEvent.ReservationsLoggedIn
				.Select(r => new ReservationEventListItemViewModel {
					Id = r.Id,
					ReservationDateTime = r.ReservationDateTime,
					NumberOfPeople = r.NumberOfPeople,
					Name = $"{r.User.LastName} {r.User.FirstName}"
				}).ToList( );

			selectedEvent.ReservationsNotLoggedIn.ForEach(r => viewModel.Reservations.Add(
				new ReservationEventListItemViewModel {
					Id = r.Id,
					ReservationDateTime = r.ReservationDateTime,
					NumberOfPeople = r.NumberOfPeople,
					Name = $"{r.LastName} {r.FirstName}"
				}));

			return View(viewModel);
		}

		//[HttpGet]
		//public IActionResult Edit( ) {
		//	return View( );
		//}
		//[HttpPost]
		//public IActionResult Edit( ) {
		//	return View( );
		//}

		[HttpGet]
		public IActionResult Delete(Guid id) {
			ReservationLoggedIn reservationloggedin = _database.FindReservationLoggedInById(id);

			if (reservationloggedin != null) {

				Event newEvent = new Event{
					Title= reservationloggedin.Event.Title
				};

				return View(new ReservationDeleteViewModel {
					Firstname = reservationloggedin.User.FirstName,
					Lastname = reservationloggedin.User.LastName,
					Id = reservationloggedin.Id,
					EventTitle = newEvent.Title,
					NumberOfPeople = reservationloggedin.NumberOfPeople
				});

			}
			ReservationNotLoggedIn reservationnotloggedin= _database.FindReservationNotLoggedInById(id);
			if (reservationnotloggedin != null) {
				return View(new ReservationDeleteViewModel { Firstname = reservationnotloggedin.FirstName, Lastname = reservationnotloggedin.LastName });
			}
			return new NotFoundResult( );
		}

		[HttpPost]
		public IActionResult ConfirmDelete(Guid id) {
			var reservationloggedin =_database.FindReservationLoggedInById(id);
			var reservationnotloggedin =_database.FindReservationNotLoggedInById(id);
			if (reservationloggedin != null) {
				_database.RemoveReservationLoggedIn(reservationloggedin);
				return RedirectToAction("Index");
			} else if (reservationnotloggedin != null) {
				_database.RemoveReservationNotLoggedIn(reservationnotloggedin);
				return RedirectToAction("Index");
			} else {
				return new NotFoundResult( );
			}
		}
	}
}