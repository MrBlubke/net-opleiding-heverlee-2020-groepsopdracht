using dotNet_Core_Groepsopdracht.Attributes;
using dotNet_Core_Groepsopdracht.Data.Database;
using dotNet_Core_Groepsopdracht.Entities;
using dotNet_Core_Groepsopdracht.Models;
using dotNet_Core_Groepsopdracht.Services.Design;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;


namespace dotNet_Core_Groepsopdracht.Controllers {
	[PermissionAuthorize(Permissions.SponsorManagement)]
	public class SponsorController : Controller {

		private readonly IDatabase _database;
		private readonly IPhotoService _photoservice;

		public SponsorController(IDatabase database, IPhotoService photoService) {
			_database = database;
			_photoservice = photoService;
		}
		public IActionResult Index( ) {

			List<SponsorViewModel> viewModel = _database.GetSponsors().Select(x => new SponsorViewModel{
				Naam=x.Naam,
				Photo = x.Photo,
				Id = x.Id,
				Postcode = x.Postcode,
				Url = x.Url
			}).ToList();
			return View(viewModel);
		}
		public IActionResult Detail(Guid id) {
			Sponsor sponsortEntity = _database.FindSponsorById(id);
			if (sponsortEntity == null) {
				return new NotFoundResult( );
			}
			SponsorViewModel sponsorViewModel = new SponsorViewModel{
				Naam = sponsortEntity.Naam,
				Photo = sponsortEntity.Photo,
				Url=sponsortEntity.Url,
				Postcode=sponsortEntity.Postcode,
				Id = sponsortEntity.Id,
				Events = sponsortEntity.SponsorEvents.Select(se => new SponsorEventViewModel {
					Id = se.Event.Id,
					Title = se.Event.Title
				}).ToList()
			};
			return View(sponsorViewModel);
		}


		[HttpGet]
		public IActionResult Create( ) {
			return View( );
		}

		[HttpPost]
		public IActionResult Create(SponsorCreateViewModel viewModel) {

			if (!TryValidateModel(viewModel)) {
				return View(viewModel);
			}
			string imageUrl = null;
			if (viewModel.Image != null) imageUrl = _photoservice.UploadedFile(viewModel.Image);
			Sponsor sponsor = new Sponsor(){
				Id = Guid.NewGuid(),
				Naam = viewModel.Naam,
				Postcode = viewModel.Postcode,
				Url = viewModel.Url,
				Photo = imageUrl,
				SponsorEvents = new List<SponsorEvent>()
			};
			_database.AddSponsor(sponsor);
			return RedirectToAction(nameof(Index));
		}
		[HttpGet]
		public IActionResult Edit(Guid id) {
			var sponsorEntity = _database.FindSponsorById(id);
			if (sponsorEntity == null) {
				return new NotFoundResult( );
			}
			var sponsorViewModel = new SponsorEditViewModel
			{
				Id = sponsorEntity.Id,
				Naam=sponsorEntity.Naam,
				Postcode=sponsorEntity.Postcode,
				Url=sponsorEntity.Url,
				Photo=sponsorEntity.Photo
			};
			return View(sponsorViewModel);
		}
		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult Edit(Guid id, SponsorEditViewModel sponsor) {
			if (TryValidateModel(sponsor)) {
				var sponsor1 = _database.FindSponsorById(id);
				if (sponsor1 == null) {
					return new NotFoundResult( );
				}
				sponsor1.Naam = sponsor.Naam;
				sponsor1.Postcode = sponsor.Postcode;
				sponsor1.Url = sponsor.Url;
				if (sponsor.Image != null) {
					string uniqueFileName = _photoservice.UploadedFile(sponsor.Image);
					sponsor1.Photo = uniqueFileName;
				}
				_database.UpdateSponsor(sponsor1, id);
				return RedirectToAction("Index", new { id = id });
			} else {
				return View(sponsor);
			}
		}
		[HttpGet]
		public IActionResult Delete(Guid id) {
			var sponsorEntity = _database.FindSponsorById(id);
			if (sponsorEntity == null) {
				return new NotFoundResult( );
			}
			var svm = new SponsorDeleteViewModel
			{
				Naam = sponsorEntity.Naam,
				Id = sponsorEntity.Id,
			};
			return View(svm);
		}
		[HttpPost]
		public IActionResult Delete(Guid id, SponsorDeleteViewModel sponsor) {
			var sponsorEntity = _database.GetSponsors().SingleOrDefault(sponsor =>sponsor.Id.Equals(id));
			if (sponsorEntity == null) {
				return new NotFoundResult( );
			}
			_database.RemoveSponsor(sponsorEntity);
			return RedirectToAction(nameof(Index));
		}
	}
}
