﻿using dotNet_Core_Groepsopdracht.Areas.Identity.Data;
using dotNet_Core_Groepsopdracht.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace dotNet_Core_Groepsopdracht.Data.Database {
	public class MemoryDatabase : IDatabase {

		#region Address

		private readonly List<Address> _addressesEntities = new List<Address>( ){
			new Address("straat",0,"1000","Brussel","5A")
		};

		public IEnumerable<Address> GetAddresses( ) {
			List<Address> adressList = _addressesEntities;

			return adressList;
		}

		public Address FindAddressById(Guid id) {
			List<Address> adressList = _addressesEntities;

			Address q = adressList.FirstOrDefault(p => p.Id == id);

			return q;
		}

		public Address FindEventAddressById(Guid id) {
			throw new NotImplementedException( );
		}

		public void AddAddress(Address address) {
			_addressesEntities.Add(address);
		}

		public void RemoveAddress(Address address) {
			_addressesEntities.Remove(address);
		}

		public void UpdateAddress(Address address, Guid id) {
			Address addressEntity = _addressesEntities.SingleOrDefault(a => a.Id.Equals(id));

			if (!(addressEntity is null)) {
				addressEntity.Street = address.Street;
				addressEntity.Number = address.Number;
				addressEntity.ZipCode = address.ZipCode;
				addressEntity.City = address.City;
			}
		}

		#endregion Address

		#region Event

		private readonly List<Event> _eventsEntities = new List<Event>( );

		public IEnumerable<Event> GetEvents( ) {
			List<Event> eventList = _eventsEntities;

			return eventList;
		}

		public Event FindEventById(Guid id) {
			List<Event> eventList = _eventsEntities;

			Event q = eventList.FirstOrDefault(p => p.Id == id);

			return q;
		}

		public void AddEvent(Event _event) {
			_eventsEntities.Add(_event);
		}

		public void RemoveEvent(Event _event) {
			_eventsEntities.Remove(_event);
		}

		public void UpdateEvent(Event _event, Guid id) {
			Event eventsEntitie = _eventsEntities.SingleOrDefault(e => e.Id.Equals(id));

			if (!(eventsEntitie is null)) {
				eventsEntitie.Title = _event.Title;
				eventsEntitie.MaxCap = _event.MaxCap;
				eventsEntitie.MinCap = _event.MinCap;
				eventsEntitie.Address = _event.Address;
				eventsEntitie.Periods = _event.Periods;
				eventsEntitie.Description = _event.Description;
				eventsEntitie.Image = _event.Image;
				eventsEntitie.Price = _event.Price;
				eventsEntitie.SponsorEvents = _event.SponsorEvents;
				eventsEntitie.ReservationsLoggedIn = _event.ReservationsLoggedIn;
				eventsEntitie.ReservationsNotLoggedIn = _event.ReservationsNotLoggedIn;
			}
		}

		#endregion Event

		#region NewsItem

		private readonly List<NewsItem> _NewsItemEntities = new List<NewsItem>( ) { };

		public IEnumerable<NewsItem> GetNewsItems( ) {
			return _NewsItemEntities;
		}

		public NewsItem FindNewsItemById(Guid id) {
			List<NewsItem> newsItemList = _NewsItemEntities;

			NewsItem q = newsItemList.FirstOrDefault(p => p.Id == id);

			return q;
		}

		public void AddNewsItem(NewsItem newsItem) {
			_NewsItemEntities.Add(newsItem);
		}

		public void RemoveNewsItem(NewsItem newsItem) {
			_NewsItemEntities.Remove(newsItem);
		}

		public void UpdateNewsItem(NewsItem newsItem, Guid id) {
			NewsItem newsItemEntitie = _NewsItemEntities.SingleOrDefault(n => n.Id.Equals(id));

			if (!(newsItemEntitie is null)) {
				newsItemEntitie.Title = newsItem.Title;
				newsItemEntitie.DateCreated = newsItem.DateCreated;
				newsItemEntitie.DateAdjusted = newsItem.DateAdjusted;
				newsItemEntitie.Content = newsItem.Content;
				newsItemEntitie.Approved = newsItem.Approved;
				newsItemEntitie.ImageUrl = newsItem.ImageUrl;
				newsItemEntitie.Author = newsItem.Author;
				newsItemEntitie.Reactions = newsItem.Reactions;
			}
		}

		#endregion NewsItem

		#region Reaction

		private readonly List<Reaction> _ReactionEntities = new List<Reaction>( ) { };

		public IEnumerable<Reaction> GetReactions( ) {
			return _ReactionEntities;
		}

		public Reaction FindReactionById(Guid id) {
			List<Reaction> reactionList = _ReactionEntities;

			Reaction q = reactionList.FirstOrDefault(p => p.Id == id);

			return q;
		}

		public void AddReaction(Reaction reaction) {
			_ReactionEntities.Add(reaction);
		}

		public void RemoveReaction(Reaction reaction) {
			_ReactionEntities.Remove(reaction);
		}

		public void UpdateReaction(Reaction reaction, Guid id) {
			Reaction reactionEntitie = _ReactionEntities.SingleOrDefault(r => r.Id.Equals(id));

			if (!(reactionEntitie is null)) {
				reactionEntitie.Title = reaction.Title;
				reactionEntitie.DateMade = reaction.DateMade;
				reactionEntitie.Author = reaction.Author;
				reactionEntitie.Content = reaction.Content;
				reactionEntitie.NewsItem = reaction.NewsItem;
			}
		}

		#endregion Reaction

		#region ReservationLoggedIn

		private readonly List<ReservationLoggedIn> _ReservationLoggedInEntities = new List<ReservationLoggedIn>( );

		public IEnumerable<ReservationLoggedIn> GetReservationsLoggedIn( ) {
			return _ReservationLoggedInEntities;
		}

		public ReservationLoggedIn FindReservationLoggedInById(Guid id) {
			List<ReservationLoggedIn> reservationList = _ReservationLoggedInEntities;

			ReservationLoggedIn q = reservationList.FirstOrDefault(p => p.Id == id);

			return q;
		}

		public void AddReservationLoggedIn(ReservationLoggedIn reservation) {
			_ReservationLoggedInEntities.Add(reservation);
		}

		public void RemoveReservationLoggedIn(ReservationLoggedIn reservation) {
			_ReservationLoggedInEntities.Remove(reservation);
		}

		public void UpdateReservationLoggedIn(ReservationLoggedIn reservation, Guid id) {
			ReservationLoggedIn ReservationLoggedInEntities = _ReservationLoggedInEntities.SingleOrDefault(_event1 => { return _event1.Id.Equals(id); });

			ReservationLoggedInEntities.User = reservation.User;
		}

		#endregion ReservationLoggedIn

		#region ReservationNotLoggedIn

		private readonly List<ReservationNotLoggedIn> _ReservationNotLoggedInEntities = new List<ReservationNotLoggedIn>( );

		public IEnumerable<ReservationNotLoggedIn> GetReservationsNotLoggedIn( ) {
			return _ReservationNotLoggedInEntities;
		}

		public ReservationNotLoggedIn FindReservationNotLoggedInById(Guid id) {
			List<ReservationNotLoggedIn> reservationList = _ReservationNotLoggedInEntities;

			ReservationNotLoggedIn q = reservationList.FirstOrDefault(p => p.Id == id);

			return q;
		}

		public void AddReservationNotLoggedIn(ReservationNotLoggedIn reservation) {
			_ReservationNotLoggedInEntities.Add(reservation);
		}

		public void RemoveReservationNotLoggedIn(ReservationNotLoggedIn reservation) {
			_ReservationNotLoggedInEntities.Remove(reservation);
		}

		public void UpdateReservationNotLoggedIn(ReservationNotLoggedIn reservation, Guid id) {
			ReservationNotLoggedIn ReservationNotLoggedInEntitie = _ReservationNotLoggedInEntities.SingleOrDefault(r => r.Id.Equals(id));

			if (!(ReservationNotLoggedInEntitie is null)) {
				ReservationNotLoggedInEntitie.FirstName = reservation.FirstName;
				ReservationNotLoggedInEntitie.LastName = reservation.LastName;
				ReservationNotLoggedInEntitie.Email = reservation.Email;
				ReservationNotLoggedInEntitie.PhoneNumber = reservation.PhoneNumber;
				ReservationNotLoggedInEntitie.Zipcode = reservation.Zipcode;
			}
		}

		#endregion ReservationNotLoggedIn

		#region Role

		private readonly List<Role> _RoleEntities = new List<Role>( );

		public IEnumerable<Role> GetRoles( ) {
			return _RoleEntities;
		}

		public Role FindRoleById(Guid id) {
			List<Role> RoleList = _RoleEntities;

			Role q = RoleList.FirstOrDefault(p => p.Id == id);

			return q;
		}

		public void AddRole(Role role) {
			_RoleEntities.Add(role);
		}

		public void RemoveRole(Role role) {
			_RoleEntities.Remove(role);
		}

		public void UpdateRole(Role role, Guid id) {
			Role roleEntitie = _RoleEntities.SingleOrDefault(r => r.Id.Equals(id));

			if (!(roleEntitie is null)) {
				roleEntitie.Name = role.Name;
				roleEntitie.Permissions = role.Permissions;
			}
		}

		#endregion Role

		#region Sponsor

		private readonly List<Sponsor> _sponsorEntities = new List<Sponsor>( );

		public IEnumerable<Sponsor> GetSponsors( ) {
			return _sponsorEntities;
		}

		public Sponsor FindSponsorById(Guid id) {
			List<Sponsor> sponsorList = _sponsorEntities;

			Sponsor q = sponsorList.FirstOrDefault(p => p.Id == id);

			return q;
		}

		public void AddSponsor(Sponsor Sponsor) {
			_sponsorEntities.Add(Sponsor);
		}

		public void RemoveSponsor(Sponsor Sponsor) {
			_sponsorEntities.Remove(Sponsor);
		}

		public void UpdateSponsor(Sponsor Sponsor, Guid id) {
			Sponsor sponsorEntitie = _sponsorEntities.SingleOrDefault(s => s.Id.Equals(id));

			if (!(sponsorEntitie is null)) {
				sponsorEntitie.Naam = Sponsor.Naam;
				sponsorEntitie.Photo = Sponsor.Photo;
				sponsorEntitie.Postcode = Sponsor.Postcode;
				sponsorEntitie.Url = Sponsor.Url;
				sponsorEntitie.SponsorEvents = Sponsor.SponsorEvents;
			}
		}

		#endregion Sponsor

		#region User

		private readonly List<User> _userEntities = new List<User>( );

		public IEnumerable<User> GetUsers( ) {
			return _userEntities;
		}

		public User FindUserById(Guid id) {
			List<User> userList = _userEntities;

			User q = userList.FirstOrDefault(p => p.UserId == id);

			return q;
		}

		public void AddUser(User user) {
			_userEntities.Add(user);
		}

		public void RemoveUser(User user) {
			_userEntities.Remove(user);
		}

		public void UpdateUser(User user, Guid id) {
			User userEntitie = _userEntities.SingleOrDefault(u => u.UserId.Equals(id));

			if (!(userEntitie is null)) {
				userEntitie.CreatedOn = user.CreatedOn;
				userEntitie.FirstName = user.FirstName;
				userEntitie.LastName = user.LastName;
				userEntitie.Address = user.Address;
				userEntitie.PhotoUrl = user.PhotoUrl;
				userEntitie.NewsItems = user.NewsItems;
				userEntitie.Reactions = user.Reactions;
				userEntitie.Reservations = user.Reservations;
				userEntitie.PhoneNumber = user.PhoneNumber;
				userEntitie.Email = user.Email;
				userEntitie.UserName = user.UserName;
				userEntitie.LockoutEnabled = user.LockoutEnabled;
				userEntitie.LockoutEnd = user.LockoutEnd;
			}
		}

		#endregion User

		#region ContactMessage

		private readonly List<Contactmessage> _contactmessageEntities = new List<Contactmessage>( );

		public IEnumerable<Contactmessage> GetContactMessages( ) {
			List<Contactmessage> contactmessageList = _contactmessageEntities;

			return contactmessageList;
		}

		public Contactmessage FindContactMessageById(Guid id) {
			List<Contactmessage> contactmessageList = _contactmessageEntities;

			Contactmessage q = contactmessageList.FirstOrDefault(p => p.Id == id);

			return q;
		}

		public void AddContactMessage(Contactmessage _contactmessage) {
			_contactmessageEntities.Add(_contactmessage);
		}

		public void RemoveContactMessage(Contactmessage _contactmessage) {
			_contactmessageEntities.Remove(_contactmessage);
		}

		public void UpdateContactMessage(Contactmessage _contactmessage, Guid id) {
			Contactmessage contactmessageEntitie = _contactmessageEntities.SingleOrDefault(e => e.Id.Equals(id));

			if (!(contactmessageEntitie is null)) {
				contactmessageEntitie.Name = _contactmessage.Name;
				contactmessageEntitie.Email = _contactmessage.Email;
				contactmessageEntitie.MessageContent = _contactmessage.MessageContent;
			}
		}

		#endregion ContactMessage

		#region Period

		public List<Period> FindPeriodsByEventId(Guid id) {
			throw new NotImplementedException( );
		}

		public List<ReservationLoggedIn> FindReservationsByUserId(Guid id) {
			throw new NotImplementedException( );
		}

		#endregion Period
	}
}