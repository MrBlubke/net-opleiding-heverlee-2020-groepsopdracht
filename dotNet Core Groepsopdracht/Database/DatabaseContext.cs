﻿using dotNet_Core_Groepsopdracht.Areas.Identity.Data;
using dotNet_Core_Groepsopdracht.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;

namespace dotNet_Core_Groepsopdracht.Database {
	public class DatabaseContext : IdentityDbContext<User, Role, Guid> {
		public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options) {

		}

		public DbSet<Address> Addresses { get; set; }
		public DbSet<Event> Events { get; set; }
		public DbSet<NewsItem> NewsItems { get; set; }
		public DbSet<Reaction> Reactions { get; set; }
		public DbSet<ReservationLoggedIn> ReservationsLoggedIn { get; set; }
		public DbSet<ReservationNotLoggedIn> ReservationsNotLoggedIn { get; set; }

		// Comment becuase this is already in the supper class
		//public DbSet<Role> Roles { get; set; }
		public DbSet<Sponsor> Sponsors { get; set; }

		// Comment because this is already in the supper class
		//public DbSet<User> Users { get; set; }
		public DbSet<Contactmessage> ContactMessages { get; set; }
		public DbSet<Period> Periods { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder) {

			#region user-newsitem
			modelBuilder.Entity<NewsItem>( )
				.HasOne(n => n.Author)
				.WithMany(u => u.NewsItems)
				//.OnDelete(DeleteBehavior.NoAction)
				.HasForeignKey(n => n.AuthorId);

			modelBuilder.Entity<User>( )
				.HasMany(u => u.NewsItems)
				.WithOne(n => n.Author)
				.HasForeignKey(a => a.AuthorId);
			#endregion

			#region newsitem-reaction

			modelBuilder.Entity<NewsItem>( )
				.HasMany(n => n.Reactions)
				.WithOne(r => r.NewsItem)
				//.OnDelete(DeleteBehavior.NoAction)
				.HasForeignKey(n => n.NewsItemId);

			modelBuilder.Entity<Reaction>( )
				.HasOne(r => r.NewsItem)
				.WithMany(n => n.Reactions)
				//.OnDelete(DeleteBehavior.NoAction)
				.HasForeignKey(r => r.NewsItemId);
			#endregion

			#region reaction-user
			modelBuilder.Entity<Reaction>( )
				.HasOne(r => r.Author)
				.WithMany(a => a.Reactions)
				.OnDelete(DeleteBehavior.NoAction)
				.HasForeignKey(a => a.AuthorId);

			modelBuilder.Entity<User>( )
				.HasMany(n => n.Reactions)

				.WithOne(u => u.Author)
				.IsRequired(false)
				.OnDelete(DeleteBehavior.NoAction)
			.HasForeignKey(n => n.AuthorId);
			#endregion

			#region event-reservations
			modelBuilder.Entity<Event>( )
				.HasMany(e => e.ReservationsLoggedIn)
				.WithOne(r => r.Event)
				.HasForeignKey(r => r.EventId);

			modelBuilder.Entity<Event>( )
				.HasMany(e => e.ReservationsNotLoggedIn)
				.WithOne(r => r.Event)
				.HasForeignKey(r => r.EventId);
			#endregion

			#region sponsorevent
			modelBuilder.Entity<SponsorEvent>( )
				.HasKey(x => new { x.EventId, x.SponsorId });
			modelBuilder.Entity<SponsorEvent>( )
				.HasOne(e => e.Sponsor)
				.WithMany(s => s.SponsorEvents)
				.HasForeignKey(s => s.SponsorId);
			modelBuilder.Entity<SponsorEvent>( )
				.HasOne(s => s.Event)
				.WithMany(e => e.SponsorEvents)
				.HasForeignKey(s => s.EventId);
			#endregion

			#region user-reservations
			modelBuilder.Entity<User>( )
				.HasMany(u => u.Reservations)
				.WithOne(r => r.User)
				.HasForeignKey(r => r.UserId);
			#endregion

			#region userrole
			/*modelBuilder
				.Entity<Role>( )
				.Property(r => r.Id)
				.ValueGeneratedOnAdd( );*/

			modelBuilder.Entity<UserRole>( )
				.HasKey(x => new { x.UserId, x.RoleId });

			modelBuilder.Entity<UserRole>( )
				.HasOne(x => x.User)
				.WithMany(s => s.UserRoles)
				.HasForeignKey(s => s.UserId);
			modelBuilder.Entity<UserRole>( )
				.HasOne(x => x.Role)
				.WithMany(s => s.UserRoles)
				.HasForeignKey(s => s.RoleId);
			#endregion

			#region event-period
			modelBuilder.Entity<Event>( )
				.HasMany(e => e.Periods)
				.WithOne(p => p.Event)
				.HasForeignKey(e => e.EventId);
			modelBuilder.Entity<Period>( )
				.HasOne(p => p.Event)
				.WithMany(e => e.Periods)
				.HasForeignKey(p => p.EventId);

			#endregion

			// To set up the identities
			base.OnModelCreating(modelBuilder);
		}
	}
}
