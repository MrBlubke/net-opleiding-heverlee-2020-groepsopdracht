﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace dotNet_Core_Groepsopdracht.Migrations
{
    public partial class FixUserReservationId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ReservationsLoggedIn_AspNetUsers_EventId",
                table: "ReservationsLoggedIn");

            migrationBuilder.AddColumn<Guid>(
                name: "UserId",
                table: "ReservationsLoggedIn",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_ReservationsLoggedIn_UserId",
                table: "ReservationsLoggedIn",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_ReservationsLoggedIn_AspNetUsers_UserId",
                table: "ReservationsLoggedIn",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ReservationsLoggedIn_AspNetUsers_UserId",
                table: "ReservationsLoggedIn");

            migrationBuilder.DropIndex(
                name: "IX_ReservationsLoggedIn_UserId",
                table: "ReservationsLoggedIn");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "ReservationsLoggedIn");

            migrationBuilder.AddForeignKey(
                name: "FK_ReservationsLoggedIn_AspNetUsers_EventId",
                table: "ReservationsLoggedIn",
                column: "EventId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
