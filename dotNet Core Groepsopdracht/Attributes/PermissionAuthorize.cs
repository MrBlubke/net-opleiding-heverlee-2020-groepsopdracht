﻿using dotNet_Core_Groepsopdracht.Areas.Identity.Data;
using dotNet_Core_Groepsopdracht.Database;
using dotNet_Core_Groepsopdracht.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dotNet_Core_Groepsopdracht.Attributes {
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
	public class PermissionAuthorize : AuthorizeAttribute, IAuthorizationFilter {
		//
		// Fields
		//
		private readonly Permissions _permission;
		private readonly PermissionAuthorizeType _authorizationType;

		//
		// Constructors
		//
		public PermissionAuthorize(Permissions permission, PermissionAuthorizeType authorizationType = PermissionAuthorizeType.HasAll) {
			_permission = permission;
			_authorizationType = authorizationType;
		}

		//
		// Properties
		//

		//
		// Methodes
		//
		public void OnAuthorization(AuthorizationFilterContext context) {
			System.Security.Claims.ClaimsPrincipal user = context.HttpContext.User;

			if (!user.Identity.IsAuthenticated) {
				// it isn't needed to set unauthorized result 
				// as the base class already requires the user to be authenticated
				// this also makes redirect to a login page work properly
				// context.Result = new UnauthorizedResult();
				return;
			}

			DatabaseContext _database = context.HttpContext.RequestServices.GetService<DatabaseContext>( );
			UserManager<User> _userManager = context.HttpContext.RequestServices.GetService<UserManager<User>>( );

			User fullUser = _database.Users.Include("UserRoles.Role").SingleOrDefault(u => u.Id.ToString() == _userManager.GetUserId(user));

			context.Result = _authorizationType switch
			{
				PermissionAuthorizeType.HasAll => !fullUser.HasPerm(_permission) ? new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden) : context.Result,
				PermissionAuthorizeType.HasAny => (_permission & fullUser.GetPermissions( )) == 0 ? new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden) : context.Result,
				_ => new StatusCodeResult((int)System.Net.HttpStatusCode.Forbidden)
			};
			return;
		}
	}


	public enum PermissionAuthorizeType {
		HasAll,
		HasAny
	}
}
