﻿using dotNet_Core_Groepsopdracht.Areas.Identity.Data;
using dotNet_Core_Groepsopdracht.Data.Database;
using dotNet_Core_Groepsopdracht.Entities;
using dotNet_Core_Groepsopdracht.Services;
using dotNet_Core_Groepsopdracht.Services.Design;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;


namespace dotNet_Core_Groepsopdracht.Areas.Identity.Pages.Account {
	[AllowAnonymous]
	public class RegisterModel : PageModel {
		private readonly SignInManager<User> _signInManager;
		private readonly UserManager<User> _userManager;
		private readonly ILogger<RegisterModel> _logger;
		private readonly IEmailSender _emailSender;
		private readonly IPhotoService _photoService;
		private readonly IDatabase _database;

		public RegisterModel(
			UserManager<User> userManager,
			SignInManager<User> signInManager,
			ILogger<RegisterModel> logger,
			IEmailSender emailSender,
			IWebHostEnvironment webHostEnvironment, IDatabase database) {
			_userManager = userManager;
			_signInManager = signInManager;
			_logger = logger;
			_emailSender = emailSender;
			_photoService = new PhotoService(webHostEnvironment);
			_database = database;
		}

		[BindProperty]
		public InputModel Input { get; set; }

		public string ReturnUrl { get; set; }

		public IList<AuthenticationScheme> ExternalLogins { get; set; }

		public class InputModel {
			[Required(ErrorMessage = "Vul een gebruikersnaam in.")]
			[StringLength(16, ErrorMessage = "De {0} moet minstens {2} en maximum {1} tekens lang zijn.", MinimumLength = 1)]
			[Display(Name = "Gebruikersnaam")]
			public string UserName { get; set; }
			[Required(ErrorMessage = "Vul een voornaam in.")]
			[StringLength(50, ErrorMessage = "De {0} moet minstens {2} en maximum {1} tekens lang zijn.", MinimumLength = 1)]
			[Display(Name = "Voornaam")]
			public string FirstName { get; set; }

			[Required(ErrorMessage = "Vul een achternaam in.")]
			[StringLength(50, ErrorMessage = "De {0} moet minstens {2} en maximum {1} tekens lang zijn.", MinimumLength = 1)]
			[Display(Name = "Achternaam")]
			public string LastName { get; set; }

			[Required(ErrorMessage = "Vul een straatnaam in.")]
			[StringLength(50, ErrorMessage = "De {0} moet minstens {2} en maximum {1} tekens lang zijn.", MinimumLength = 1)]
			[Display(Name = "Straat")]
			public string Street { get; set; }

			[Required(ErrorMessage = "Vul een huisnummer in.")]
			[Display(Name = "Nummer")]
			public int Number { get; set; }

			[StringLength(4, ErrorMessage = "De {0} moet minstens {2} en maximum {1} tekens lang zijn.", MinimumLength = 1)]
			[Display(Name = "Bus")]
			public string Bus { get; set; }

			[Required(ErrorMessage = "Vul een postcode in.")]
			[StringLength(6, ErrorMessage = "De {0} moet minstens {2} en maximum {1} tekens lang zijn.", MinimumLength = 1)]
			[Display(Name = "Postcode")]
			public string ZipCode { get; set; }

			[Required(ErrorMessage = "Vul een stad in.")]
			[StringLength(50, ErrorMessage = "De {0} moet minstens {2} en maximum {1} tekens lang zijn.", MinimumLength = 1)]
			[Display(Name = "Stad")]
			public string City { get; set; }

			[Required(ErrorMessage = "Vul een email adres in.")]
			[EmailAddress]
			[Display(Name = "Email")]
			public string Email { get; set; }

			[Required(ErrorMessage = "Vul een wachtwoord in")]
			[StringLength(100, ErrorMessage = "De {0} moet minstens {2} en maximum {1} tekens lang zijn.", MinimumLength = 6)]
			[DataType(DataType.Password)]
			[Display(Name = "Wachtwoord")]
			public string Password { get; set; }

			[DataType(DataType.Password)]
			[Display(Name = "Bevestig wachtwoord")]
			[Compare("Password", ErrorMessage = "De wachtwoorden komen niet overeen.")]
			public string ConfirmPassword { get; set; }

			[Display(Name = "Profielfoto (max. 2 MB)")]
			public IFormFile PhotoUrl { get; set; }
		}

		public async Task OnGetAsync(string returnUrl = null) {
			ReturnUrl = returnUrl;
			ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync( )).ToList( );
		}

		public async Task<IActionResult> OnPostAsync(string returnUrl = null) {
			returnUrl = returnUrl ?? Url.Content("~/");
			ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync( )).ToList( );

			// TODO: #50 issue fix here
			if (DoesEmailExist(Input.Email) == true) {
				if (ModelState.IsValid) {
					Address adress = new Address {
						Id = Guid.NewGuid(),
						Street = Input.Street,
						Number = Input.Number,
						Bus = Input.Bus,
						City = Input.City,
						ZipCode = Input.ZipCode
					};


					User user = new User {
						Id = Guid.NewGuid(),
						UserName = Input.UserName,
						Email = Input.Email,
						FirstName = Input.FirstName,
						LastName = Input.LastName,
						CreatedOn = DateTime.Now,
						Address = adress
					};
					bool PhotoOk = true;
					if (Input.PhotoUrl != null) {
						try {
							string uniqueFileName = _photoService.UploadedFile(Input.PhotoUrl, 2097152);
							user.PhotoUrl = uniqueFileName;
						} catch (Exception e) {
							ModelState.AddModelError(string.Empty, e.Message);
							PhotoOk = false;
						}
					}
					IdentityResult result = new IdentityResult();
					if (PhotoOk) {
						result = await _userManager.CreateAsync(user, Input.Password);
						if (result.Succeeded) {
							_logger.LogInformation("User created a new account with password.");

							string code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
							code = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(code));
							string callbackUrl = Url.Page(
						"/Account/ConfirmEmail",
						pageHandler: null,
						values: new { area = "Identity", userId = user.Id, code = code, returnUrl = returnUrl },
						protocol: Request.Scheme);

							await _emailSender.SendEmailAsync(Input.Email, "Confirm your email",
								$"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");

							if (_userManager.Options.SignIn.RequireConfirmedAccount) {
								return RedirectToPage("RegisterConfirmation", new { email = Input.Email, returnUrl = returnUrl });
							} else {
								await _signInManager.SignInAsync(user, isPersistent: false);
								return LocalRedirect(returnUrl);
							}
						}
					}
					foreach (IdentityError error in result.Errors) {
						ModelState.AddModelError(string.Empty, error.Description);
					}
				}
			}



			// If we got this far, something failed, redisplay form
			return Page( );
		}


		public bool DoesEmailExist(string email) {
			bool x = true;
			foreach (User u in _database.GetUsers( )) {
				if (u.Email.ToLower( ) == email.ToLower( )) {
					ModelState.AddModelError(string.Empty, "Email bestaat al.");
					x = false;
				}
			}
			return x;
		}
	}
}
