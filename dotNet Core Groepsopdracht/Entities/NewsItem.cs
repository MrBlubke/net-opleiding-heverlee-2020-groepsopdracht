﻿using dotNet_Core_Groepsopdracht.Areas.Identity.Data;
using System;
using System.Collections.Generic;

namespace dotNet_Core_Groepsopdracht.Entities {

	public class NewsItem {
		public Guid Id { get; set; }
		public string Title { get; set; }
		public DateTime DateCreated { get; set; }
		public DateTime? DateAdjusted { get; set; }
		public string Content { get; set; }

		// TODO: Is a bool the correct way to do this?
		// How about Status attribute with a few possible states?
		// Draft submitted: "To be reviewed" -> Newsitem: "Approved" -> NewsItem: "Being edited" -> Edited newsitem: "To be approved" -> ...
		public bool Approved { get; set; }

		public string ImageUrl { get; set; }

		public Guid AuthorId { get; set; }
		public User Author { get; set; }
		public List<Reaction> Reactions { get; set; }
	}
}