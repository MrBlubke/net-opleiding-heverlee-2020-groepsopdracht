﻿using System;

namespace dotNet_Core_Groepsopdracht.Entities {
	public class SponsorEvent {
		public Guid Id { get; set; }
		public Guid SponsorId { get; set; }
		public Guid EventId { get; set; }

		public Event Event { get; set; }
		public Sponsor Sponsor { get; set; }

	}
}
