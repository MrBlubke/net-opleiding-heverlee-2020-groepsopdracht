﻿using dotNet_Core_Groepsopdracht.Areas.Identity.Data;
using System;

namespace dotNet_Core_Groepsopdracht.Entities {
	public abstract class Reservation {
		public Guid Id { get; set; }
		public Event Event { get; set; }
		public Guid EventId { get; set; }
		public DateTime ReservationDateTime { get; set; }
		public DateTime CreatedDateTime { get; set; }
		public int NumberOfPeople { get; set; }
	}

	public class ReservationLoggedIn : Reservation {
		public Guid UserId { get; set; }
		public User User { get; set; }
	}

	public class ReservationNotLoggedIn : Reservation {
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Email { get; set; }
		public string PhoneNumber { get; set; }
		public string Zipcode { get; set; }
	}
}
