﻿using dotNet_Core_Groepsopdracht.Areas.Identity.Data;
using System;

namespace dotNet_Core_Groepsopdracht.Entities {
	public class Reaction {


		public Guid Id { get; set; }
		public string Title { get; set; }
		public DateTime DateMade { get; set; }
		public User Author { get; set; }
		public Guid AuthorId { get; set; }
		public string Content { get; set; }
		public NewsItem NewsItem { get; set; }
		public Guid NewsItemId { get; set; }


	}


}
