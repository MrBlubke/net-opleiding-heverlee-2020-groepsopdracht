﻿using System;

namespace dotNet_Core_Groepsopdracht.Entities {
	public class Address {
		public Address(string street, int number, string zipCode, string city, string bus = "") {
			Id = Guid.NewGuid( );
			Street = street;
			Number = number;
			ZipCode = zipCode;
			City = city;
			Bus = bus;
		}
		public Address( ) {

		}

		public Guid Id { get; set; }

		public string Street { get; set; }

		public int Number { get; set; }

		public string Bus { get; set; }

		public string ZipCode { get; set; }

		public string City { get; set; }
	}
}
