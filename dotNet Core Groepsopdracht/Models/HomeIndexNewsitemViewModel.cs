﻿using System;

namespace dotNet_Core_Groepsopdracht.Models {
	public class HomeIndexNewsitemViewModel {
		public Guid NewsItemId { get; set; }
		public string Title { get; set; }
		public string ImageUrl { get; set; }
	}
}
