﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace dotNet_Core_Groepsopdracht.Models {

	public class EventDeleteViewModel {
		public Guid Id { get; set; }

		[DisplayName("Titel")]
		public String Title { get; set; }

		[DisplayName("Maximum capaciteit")]
		public int MaxCap { get; set; }

		[DisplayName("Minimum capaciteit")]
		public int MinCap { get; set; }

		[DisplayName("Adres")]
		public AddressViewModel Address { get; set; }

		[DisplayName("Evenement periodes")]
		public DateTime[] PeriodeEvenement { get; set; }

		[DisplayName("Beschrijving")]
		public string Description { get; set; }

		[DisplayName("Afbeelding")]
		public string Image { get; set; }

		// TODO: Datatype prijs aanpassen?!
		[DisplayName("Prijs")]
		public string Price { get; set; }

		[DisplayName("Sponsorlijst")]
		public List<SponsorViewModel> Sponsors { get; set; }
	}
}