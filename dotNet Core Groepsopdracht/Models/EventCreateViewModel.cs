﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace dotNet_Core_Groepsopdracht.Models {

	public class EventCreateViewModel {

		public List<PeriodViewModel> PeriodLijst { get; set; } = new List<PeriodViewModel>( );

		public Guid Id { get; set; }

		[Required(AllowEmptyStrings = false, ErrorMessage = "Titel is vereist")]
		[DisplayName("Titel*")]
		[StringLength(100, ErrorMessage = "Titel is te lang")]
		public string Title { get; set; }

		[Required]
		[Range(0, Int32.MaxValue, ErrorMessage = "Maximum capaciteit kan niet lager dan 0 zijn")]
		[DisplayName("Maximum capaciteit*")]
		public int MaxCap { get; set; }

		[Required]
		[Range(0, Int32.MaxValue, ErrorMessage = "Minimum capaciteit kan niet lager dan 0 zijn")]
		[DisplayName("Minimum capaciteit*")]
		public int MinCap { get; set; }

		[Required(AllowEmptyStrings = false, ErrorMessage = "Beschrijving is vereist")]
		[DisplayName("Beschrijving*")]
		[StringLength(500, ErrorMessage = "Beschrijving is te lang")]
		public string Description { get; set; }

		[DisplayName("Afbeelding")]
		public IFormFile Image { get; set; }

		// TODO: Price as string??
		// Datatype prijs aanpassen?!
		[Required]
		[DisplayName("Prijs*")]
		public string Price { get; set; }

		public List<SponsorViewModel> Sponsors { get; set; }

		/// <summary>
		/// Periode
		/// TODO: Wat bij meerdere dagen met verschillende uren?
		/// TODO: E.g. event repeats one day a week for a few weeks
		/// </summary>
		///
		/// StartDatum cannot be later dan EndDatum => Is this a TODO? Or a statement?
		/// 

		/// <summary>
		/// Address
		/// </summary>
		[Required]
		[DisplayName("Straatnaam*")]
		[StringLength(100, ErrorMessage = "Straatnaam is te lang")]
		public string Street { get; set; }

		[Required]
		[Range(1, int.MaxValue, ErrorMessage = "Huisnummer kan niet kleiner zijn dan 1")]
		[DisplayName("Huisnummer*")]
		public int Number { get; set; }

		[DisplayName("Bus")]
		public string Bus { get; set; }

		[Required]
		[DisplayName("Postcode*")]
		public string ZipCode { get; set; }

		[Required]
		[DisplayName("Gemeente*")]
		[StringLength(100, ErrorMessage = "Naam is te lang")]
		public string City { get; set; }

		//Kijkt of startdatum vroeger is dat einddatum
		public bool PeriodIsCorrect(PeriodViewModel pvm) {
			if (pvm.Start <= pvm.End) {
				return true;
			}
			return false;
		}
	}
}