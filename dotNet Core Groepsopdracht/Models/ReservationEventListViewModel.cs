﻿using System;
using System.Collections.Generic;

namespace dotNet_Core_Groepsopdracht.Models {
	public class ReservationEventListViewModel {

		public Guid EventId { get; set; }
		public string EventTitle { get; set; }
		public string Image { get; set; }
		public string Address { get; set; }
		public List<ReservationEventListItemViewModel> Reservations { get; set; }

	}

	public class ReservationEventListItemViewModel {
		public Guid Id { get; set; }
		public string Name { get; set; }
		public DateTime ReservationDateTime { get; set; }
		public int NumberOfPeople { get; set; }

	}
}
