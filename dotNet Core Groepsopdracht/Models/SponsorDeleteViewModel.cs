﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dotNet_Core_Groepsopdracht.Models {
	public class SponsorDeleteViewModel {

		public Guid Id { get; set; }
		public string Naam { get; set; }
	}
}
