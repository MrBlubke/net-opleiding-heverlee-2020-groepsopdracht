﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dotNet_Core_Groepsopdracht.Models {
	public class EventPeriodViewModel {

		public DateTime Start { get; set; }

		public DateTime End { get; set; }

		public EventPeriodViewModel() {

		}
	}
}
