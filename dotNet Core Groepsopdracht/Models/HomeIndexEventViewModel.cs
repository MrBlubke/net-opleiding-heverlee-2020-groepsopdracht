﻿using System;

namespace dotNet_Core_Groepsopdracht.Models {
	public class HomeIndexEventViewModel {

		public Guid EventId { get; set; }
		public string Title { get; set; }
		public string Description { get; set; }
		public string EventImage { get; set; }
	}
}
