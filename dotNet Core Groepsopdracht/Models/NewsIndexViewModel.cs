﻿using dotNet_Core_Groepsopdracht.Areas.Identity.Data;
using dotNet_Core_Groepsopdracht.Entities;
using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel;

namespace dotNet_Core_Groepsopdracht.Models {

	public class NewsIndexViewModel {
		public Guid Id { get; set; }

		[DisplayName("Titel")]
		public string Title { get; set; }

		// TODO: ?? Not used in View at this point => remove?
		[DisplayName("Datum aangemaakt")]
		public DateTime DateCreated { get; set; }

		// TODO: ?? Not used in View at this point => remove?
		[DisplayName("Laatst aangepast op")]
		public DateTime DateAdjusted { get; set; }

		[DisplayName("Inhoud")]
		public string Content { get; set; }

		// TODO: ?? Not used in View at this point => remove?
		[DisplayName("Keuring")]
		public bool Approved { get; set; }

		[DisplayName("Afbeelding URL")]
		public string ImageUrl { get; set; }

		// TODO: ?? Not used in View at this point => remove?
		[DisplayName("Afbeelding")]
		public IFormFile Image { get; set; }

		[DisplayName("Auteur")]
		public User Author { get; set; }

		// TODO: ?? Not used in View at this point => remove? + Other entity!
		[DisplayName("Reactie")]
		public Reaction Reaction { get; set; }
	}
}