﻿using dotNet_Core_Groepsopdracht.Areas.Identity.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace dotNet_Core_Groepsopdracht.Models {

	public class ContactViewModel {

		public Guid Id { get; set; }

		[Required]
		[StringLength(50, ErrorMessage = "Naam is te lang")]
		[DisplayName("Naam")]
		public string Name { get; set; }

		[Required]
		[EmailAddress]
		[StringLength(100, ErrorMessage = "Email is te lang")]
		[DisplayName("Email")]
		public string Email { get; set; }

		[Required]
		[StringLength(500, ErrorMessage = "Bericht is te lang")]
		[DisplayName("Bericht")]
		public string Message { get; set; }
	}
}