﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace dotNet_Core_Groepsopdracht.Models {
	public abstract class ReservationCreateViewModel {

		public string EventTitle { get; set; }
		public Guid EventId { get; set; }
		public int Interval { get; set; } // The amount of time in between possible reservation times
		[DisplayName("Kies:")]
		public string ReservationDate { get; set; }
		public List<SelectListItem> PossibleDates { get; set; }
		public List<string> Periods { get; set; }
		[DisplayName("Specifieer het tijdstip:")]
		public string ReservationTime { get; set; }
		public List<string[]> PossibleTimes { get; set; }
		[DisplayName("Aantal personen")]
		[Range(1, int.MaxValue, ErrorMessage = "Enkel positieve nummers toegestaan")]
		public int NumberOfPeople { get; set; }
	}

	public class ReservationCreateLoggedInViewModel : ReservationCreateViewModel {
		public string UserName { get; set; }
	}

	public class ReservationCreateNotLoggedInViewModel : ReservationCreateViewModel {

		//info over user:
		[DisplayName("Voornaam*")]
		[Required(AllowEmptyStrings = false, ErrorMessage = "Voornaam is vereist")]
		public string FirstName { get; set; }

		[Required(AllowEmptyStrings = false, ErrorMessage = "Achternaam is vereist")]
		[DisplayName("Achternaam*")]
		public string LastName { get; set; }

		[Required]//TODO: nederlandstalige errormessages
		[EmailAddress]
		[DisplayName("Email*")]
		public string Email { get; set; }

		[Required]
		[Phone]
		[DisplayName("Telefoonnummer*")]
		public string PhoneNumber { get; set; }

		[DisplayName("Postcode")]
		[StringLength(4)]
		public string Zipcode { get; set; }
	}
}
