﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace dotNet_Core_Groepsopdracht.Models {
	public class SponsorCreateViewModel {

		[Required(AllowEmptyStrings = false, ErrorMessage = "Naam sponsor is vereist")]
		[DisplayName("Naam*")]
		[StringLength(50, ErrorMessage = "Naam is te lang")]
		public string Naam { get; set; }

		[DisplayName("Logo*")]
		[Required(ErrorMessage = "Logo is vereist")]
		public IFormFile Image { get; set; }

		[DisplayName("Postcode")]
		[StringLength(4, ErrorMessage = "Postcode is te lang")]
		public string Postcode { get; set; }

		[DisplayName("Link naar website")]
		public string Url { get; set; }
	}
}
