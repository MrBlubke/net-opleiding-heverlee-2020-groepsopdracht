﻿using dotNet_Core_Groepsopdracht.Entities;
using System;
using System.Collections.Generic;

namespace dotNet_Core_Groepsopdracht.Models {
	public class UserReservationListViewModel {

		public Guid UserId { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public List<ReservationLoggedIn> Reservations { get; set; }
	}
}

