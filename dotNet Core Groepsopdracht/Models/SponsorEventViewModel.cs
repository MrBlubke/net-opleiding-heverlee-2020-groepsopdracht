﻿using System;
using System.Collections.Generic;

namespace dotNet_Core_Groepsopdracht.Models {
	public class SponsorEventViewModel {
		public Guid Id { get; set; }
		public string Title { get; set; }

	}
}
