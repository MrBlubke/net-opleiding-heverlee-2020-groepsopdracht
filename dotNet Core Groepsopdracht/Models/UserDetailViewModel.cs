﻿using dotNet_Core_Groepsopdracht.Areas.Identity.Data;
using dotNet_Core_Groepsopdracht.Entities;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;

namespace dotNet_Core_Groepsopdracht.Models {
	public class UserDetailViewModel {

		public Guid UserId { get; set; }
		public string UserName { get; set; }
		public DateTime CreatedOn { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Email { get; set; }

		public string PhoneNumber { get; set; }
		public AddressViewModel Address { get; set; }
		public string PhotoUrl { get; set; }

		public List<SelectListItem> Roles { get; set; }


		public List<NewsIndexViewModel> NewsItems { get; set; }
		// public List<Reaction> Reactions { get; set; }
		public List<ReservationIndexViewModel> Reservations { get; set; }

		// TODO: this model will need to be changed for #68
		public string[] SelectedRoles { get; set; }

		public UserDetailViewModel( ) { }
		public UserDetailViewModel(User user) {
			UserId = user.UserId;
			UserName = user.UserName;
			CreatedOn = user.CreatedOn;
			FirstName = user.FirstName;
			LastName = user.LastName;
			Email = user.Email;
			PhoneNumber = user.PhoneNumber;
			if (user.Address != null) {
				Address = new AddressViewModel( ) {
					Id = user.Address.Id,
					Street = user.Address.Street,
					Number = user.Address.Number,
					Bus = user.Address.Bus,
					ZipCode = user.Address.ZipCode,
					City = user.Address.City,
				};
			}
			PhotoUrl = user.PhotoUrl;
			NewsItems = new List<NewsIndexViewModel>( );
			foreach (NewsItem item in user.NewsItems) {
				NewsItems.Add(new NewsIndexViewModel( ) {
					Id = item.Id,
					Title = item.Title,
					DateCreated = item.DateCreated,
					ImageUrl = item.ImageUrl
				});
			}
			Reservations = new List<ReservationIndexViewModel>( );
			foreach (Reservation r in user.Reservations) {
				Reservations.Add(new ReservationIndexViewModel( ) {
					Id = r.Id,
					EventTitle = r.Event.Title,
					ReservationDateTime = r.ReservationDateTime,
					NumberOfPeople = r.NumberOfPeople
				});
			}
			SelectedRoles = new string[user.UserRoles.Count];
			for (int i = 0; i < user.UserRoles.Count; i++) {
				SelectedRoles[i] = user.UserRoles[i].Role.Name;
			}
		}
	}
}
