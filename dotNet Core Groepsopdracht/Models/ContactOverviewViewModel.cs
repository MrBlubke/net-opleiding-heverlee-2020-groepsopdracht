﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dotNet_Core_Groepsopdracht.Models {
	public class ContactOverviewViewModel {
		public Guid Id { get; set; }
		public string Name { get; set; }
		public string Email { get; set; }
		public string MessageContent { get; set; }
	}
}
