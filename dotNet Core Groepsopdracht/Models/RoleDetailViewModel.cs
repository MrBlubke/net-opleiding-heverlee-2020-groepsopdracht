﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dotNet_Core_Groepsopdracht.Models {
	public class RoleDetailViewModel {
		public Guid Id { get; set; }
		public string Name { get; set; }
		public string Permissions { get; set; }


		public List<Guid> TopUserIds { get; set; }
		public List<string> TopUserNames { get; set; }
		

		
	}
}
